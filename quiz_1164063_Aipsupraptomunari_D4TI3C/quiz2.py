import shapefile					#untuk mengambil data modul pada shapefile
w=shapefile.Writer()				#untuk memasang fungsi writer
w.shapeType							#untuk mengaktifkan type shape

w.field("kolom1","C")				#untuk membuat kolom dengan menggunakan type data char
w.field("kolom2","C")

w.record("gskuy","ghola")			#kolom yang dibuat aka diisikan recordnya sesuai dengan kolom yang ada pada setiap ruang dengan nama unik dan lucu karena recordnya ada 6 maka bentuk koordinat juga ada 6
w.record("gcaw","gwhicis")
w.record("gkuy","gliving")




w.poly(parts=[[[1,1],[4,1],[1,3]]],shapeType=shapefile.POLYLINE)	#membuat koordinat berbemtuk polyline seperti segita siku-siku 3 buah 
w.poly(parts=[[[-1,1],[-4,1],[-1,3]]],shapeType=shapefile.POLYLINE)	#gcaw
w.poly(parts=[[[1,-1],[4,-1],[1,-3]]],shapeType=shapefile.POLYLINE)	#gkuy



w.save("quiz2")